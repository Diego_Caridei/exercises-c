//
//  main.cpp
//  NameSpace
//
//  Created by Diego Caridei on 02/12/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
using namespace std;
namespace firstNums {
    int num1=10;
    int num2=12;
}
namespace secondNums {
    int num1=1;
    int num2=2;
}


int main(int argc, const char * argv[]) {
    
    cout<<"Numero Uno"<< firstNums::num1<<endl;
    cout<<"Numero Uno"<< secondNums::num1<<endl;

    {
    using namespace firstNums;
    cout <<"Numero uno "<<num1<<endl;
    }
    {
    using namespace secondNums;
    cout <<"Numero uno "<<num1<<endl;
    }


        return 0;
}
