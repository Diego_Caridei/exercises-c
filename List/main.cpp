//
//  main.cpp
//  Liste1
//
//  Created by Diego Caridei on 01/12/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
#include <list>
using namespace::std;
int main(int argc, const char * argv[]) {
 
    list<string> nomi;
    
    nomi.push_back("Diego");
    nomi.push_back("Giorgio");
    nomi.push_back("Jhonny");
    nomi.push_back("Antonio");
    nomi.push_back("Barbara");
    nomi.push_back("Zorro");
    nomi.push_back("Ciro");

    

    //Per ciclare una lista
    list<string>::iterator iterator = nomi.begin();
    while (iterator!=nomi.end() ){
        cout<< *iterator<<endl;
        iterator++;
    }
    cout<<endl;
    cout<<endl;

    cout<<"Primo elemento: "<<nomi.front()<<endl;
    cout<<"Ultimo elemento: "<<nomi.back()<<endl;

    nomi.reverse();
    cout<<"Stampa al contrario"<<endl;
    cout<<endl;
    cout<<endl;
    iterator=nomi.begin();
    while (iterator!=nomi.end() ){
        cout<< *iterator<<endl;
        iterator++;
    }
    
    nomi.sort();
    cout<<"Stampa ordinata"<<endl;
    cout<<endl;
    cout<<endl;
    iterator=nomi.begin();
    while (iterator!=nomi.end() ){
        cout<< *iterator<<endl;
        iterator++;
    }
    
    nomi.push_front("Armando");
    
    nomi.remove("Armando");
    
    return 0;
}
