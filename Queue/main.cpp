//
//  main.cpp
//  Coda
//
//  Created by Diego Caridei on 01/12/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
#include <deque>
using namespace std;

int main(int argc, const char * argv[]) {
    deque<string> line;
    line.push_back("Cliente 1");
    line.push_front("Cliente 2");
    line.push_back("Cliente 3");
    for (int i=0; i<line.size(); i++) {
        cout<<line[i]<<endl;
    }
    
    line.pop_back();
    line.pop_front();
    for (int i=0; i<line.size(); i++) {
        cout<<line[i]<<endl;
    }

    
    return 0;
}
