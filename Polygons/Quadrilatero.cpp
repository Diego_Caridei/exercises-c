//
//  Quadrilatero.cpp
//  Esercizio2Poligoni
//
//  Created by Diego Caridei on 27/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include "Quadrilatero.h"
Quadrilatero:: Quadrilatero(double s1,double s2, double s3, double s4)
{
    side1 =s1;
    side2=s2;
    side3=s3;
    side4=s4;
}
void Quadrilatero:: display(){
    cout<<"Quadrilatero con lati: "<<side1<<" "<<side2<<" "<<side3<<" "<<side4<<endl;
}
