//
//  Trapezoidale.h
//  Esercizio2Poligoni
//
//  Created by Diego Caridei on 27/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//


#include "Quadrilatero.h"
class Trapezoidale : public Quadrilatero{
public:
    Trapezoidale(double s1, double s2, double s3 ,double s4);
};
