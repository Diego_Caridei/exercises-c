//
//  Quadrato.h
//  Esercizio2Poligoni
//
//  Created by Diego Caridei on 27/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//


#include "Quadrilatero.h"
class Quadrato:public Quadrilatero{
public:
    Quadrato(double side);
};
