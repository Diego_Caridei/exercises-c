//
//  Quadrilatero.h
//  Esercizio2Poligoni
//
//  Created by Diego Caridei on 27/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//


#include <iostream>
using namespace::std;
class Quadrilatero{
protected:
    double side1,side2,side3,side4;
public:
    Quadrilatero(double s1,double s2, double s3, double s4);
    void display();
};

