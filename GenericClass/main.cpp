//
//  main.cpp
//  GenericClass
//
//  Created by Diego Caridei on 29/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
#include "Stack.h"
int main(int argc, const char * argv[]) {
    Stack<int> numbers;
    numbers.push(12);
    numbers.push(22);
    cout<<numbers.peek()<<endl;
 int val =numbers.pop();
    cout<<numbers.peek()<<endl;

    return 0;
}
