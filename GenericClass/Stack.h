//
//  Stack.h
//  GenericClass
//
//  Created by Diego Caridei on 29/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
using namespace std;
template <class T>



class Stack {
private:
    T datastore[100];
    int top;
public:
    Stack();
    void push(int num);
    T pop();
    T peek();
    
};