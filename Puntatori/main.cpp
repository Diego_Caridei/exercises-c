//
//  main.cpp
//  Puntatori
//
//  Created by Diego Caridei on 24/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
using namespace std;
void scambia(int &n1, int &n2){
    int  temp =n1;
    n1=n2;
    n2=temp;
}
int main(int argc, const char * argv[]) {
    int  n1,n2 =0;
    cout<<"Inserisci n1\n";
    cin>>n1;
    cout<<"Inserisci n2\n";
    cin>>n2;
    cout<<"Prima dello scambio\n";
    cout<<"N1="<<n1<<" N2="<<n2<<endl;
    scambia(n1, n2);
    cout<<"Dopo lo scambio\n";
    cout<<"N1="<<n1<<" N2="<<n2<<endl;
    cout<<endl;
    
    int numero =1;
    int *pnt = &numero;
    cout<<pnt<<endl;//Stampa l'indirizzo
    cout<<*pnt<<endl;//Stampa il valore

    return 0;
}
