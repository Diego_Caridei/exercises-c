//
//  Impiegato.cpp
//  Ereditarita1
//
//  Created by Diego Caridei on 26/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include "Impiegato.h"
Impiegato::Impiegato(){
    name="";
    pagamento=0;
}
Impiegato::~Impiegato(){
    cout<<"Distruttore Impiegato"<<endl;
}

Impiegato::Impiegato(string nome,double stip){
    name=nome;
    pagamento=stip;
}
string Impiegato::getName(){
    return name;
}
void Impiegato::setName(string nam){
    name =nam;
}

double Impiegato::getPag(){
    return pagamento;
}
void Impiegato::setPag(double pag){
    pagamento = pag;
}
string Impiegato::toSting(){
    stringstream stm;
    stm<< name<<": "<<pagamento;
    return stm.str();
}


