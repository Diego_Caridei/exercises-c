//
//  Impiegato.h
//  Ereditarita1
//
//  Created by Diego Caridei on 26/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//


#include <iostream>
#include <sstream>

using namespace::std;
class Impiegato {
private:
    string name;
    double pagamento;
public:
    Impiegato();
    Impiegato(string nome,double stip);
    ~Impiegato();

    string getName();
    void setName(string nam);
    double getPag();
    void setPag(double pag);
    string toSting();

};
