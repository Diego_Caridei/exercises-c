//
//  main.cpp
//  Ereditarita1
//
//  Created by Diego Caridei on 26/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
#include "Manager.h"
using namespace::std;
int main(int argc, const char * argv[]) {
 
    Impiegato *imp1= new Impiegato("Diego Caridei",15.00);
    Impiegato imp2("Bill Brown",3200);
    cout<< imp1->toSting()<<endl;
    cout<< imp2.toSting()<<endl;
    //Classe ereditata
    Manager manager ("Frank ", 1200, true);
    //getName è un metodo della superclasse
    cout <<manager.getName()<<endl;

    return 0;
}
