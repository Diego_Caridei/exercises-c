//
//  Manager.h
//  Ereditarita1
//
//  Created by Diego Caridei on 26/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
#include "Impiegato.h"

class Manager : public Impiegato{
private:
    bool salario;
public:
    Manager(string name,double pagame, bool salary);
    bool getSalario();
    ~Manager();
    
};