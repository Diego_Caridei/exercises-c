//
//  main.cpp
//  Generic
//
//  Created by Diego Caridei on 29/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
#include <string>
using namespace::std;

template <typename T>


void stampa(T ar[],int s){
    for (int i =0; i<s; i++) {
        cout<<ar[i]<<endl;
    }
}

int main(int argc, const char * argv[]) {
    int size=10;
    int array[size];
    for (int i =0; i<size; i++) {
        array[i]=i+1;
    }
    stampa(array,size);
    string nomi []={"Diego","Frank","Giorgio",
                    "Elenena","Paquale",
                    "Nicola","Giuseppe"};
    stampa(nomi, 7);
    
    
    
    return 0;
}
