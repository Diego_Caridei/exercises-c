//
//  main.cpp
//  GenericFunction
//
//  Created by Diego Caridei on 01/12/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
using namespace::std;
template <typename T, typename U>

T maxi(T arg1,U arg2){
    if (arg1>arg2) {
        return arg1;
    }
    return arg2;
}


int main(int argc, const char * argv[]) {
    
    double num1= 35.44;
    int num2 =10;
    cout<<maxi(num1,num2)<<endl;
    
    return 0;
}
