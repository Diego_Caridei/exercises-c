//
//  main.cpp
//  Maps(Dizionari)
//
//  Created by Diego Caridei on 01/12/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
#include <map>
using namespace std;
int main(int argc, const char * argv[]) {
  
    //Chiave Valore
    /*
    map<string, string> numeri;
    numeri["Jones"]="365";
    numeri["Smith"]="467";
    numeri["Brown"]="111";
    
    cout<< "Jones: "<<numeri["Jones"]<<endl;
    cout<< "Jones: "<<numeri.size()<<endl;
    numeri.erase("Smith");
    cout<< "Jones: "<<numeri.size()<<endl;
     */
    
    map<string, int>voti;
    voti["Matematica"]= 19;
    voti["Programmazione"]= 30;
    voti["Fisica"]= 30;
    
    int somma=0;
    map<string, int>::iterator it = voti.begin();
    while (it!=voti.end()) {
        //Punta all'elemento successivo
        somma+=it->second;
    }

    
    
    

    
    return 0;
}
