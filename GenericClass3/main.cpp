//
//  main.cpp
//  GenericClass3
//
//  Created by Diego Caridei on 01/12/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
#include <vector>
using namespace::std;
template<typename T>
class List{
private:
    vector<T> dataStore;
public:
    List(){}
    void add(T item){
        dataStore.push_back(item);
    }
    void display(){
        for (int i=0; i< dataStore.size(); i++) {
            cout<<dataStore[i]<<endl;
        }
    }
    
};
int main(int argc, const char * argv[]) {
    List<string> grocery;
    grocery.add("Milg");
    grocery.add("ggs");
    grocery.add("Bread");
    grocery.display();
    
    
    return 0;
}
