//
//  Animal.h
//  Poliformismo Esercizio1
//
//  Created by Diego Caridei on 28/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include<iostream>
using namespace::std;

class Animal {
public:
    virtual void talk()=0;
    
};
