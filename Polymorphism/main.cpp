//
//  main.cpp
//  Poliformismo Esercizio1
//
//  Created by Diego Caridei on 28/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
#include "Dog.h"
#include "Cat.h"
int main(int argc, const char * argv[]) {
    Dog fido;
    Cat kitty;
    fido.talk();
    kitty.talk();
    return 0;
}
