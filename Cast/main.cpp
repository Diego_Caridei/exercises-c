//
//  main.cpp
//  Cast
//
//  Created by Diego Caridei on 03/12/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
using namespace::std;
int main(int argc, const char * argv[]) {

    //Cast implicito
    int num1= 5+12.75; //17
    
    //Cast espliotivo
    int num2 = 5+static_cast<int>(12.75);
    return 0;
}
