//
//  Nome.h
//  Classi
//
//  Created by Diego Caridei on 25/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
#include <string>
using namespace::std;

class Nome {
private:
    string first,middle,last;
public:
    Nome(string nome,string mname,string lname);
    ~Nome();
    string toString();
    
};