//
//  main.cpp
//  Namespace2
//
//  Created by Diego Caridei on 02/12/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
using namespace::std;

namespace minMax {
    
    int min(int num1,int num2){
        if (num1<num2) {
            return num1;
        }
        else{
            return num2;
        }
    }
    int max(int num1,int num2){
        if (num1>num2) {
            return num1;
        }
        else{
            return num2;
        }
    }
    
}
namespace People {
    class Person{
    private:
        string name;
        string sex;
    public:
        Person(){}
        ~Person(){
            cout<<"Distrutto"<<endl;
        }
        Person(string n,string s){
            name=n;
            sex=s;
        }
        string get(){
            return name+" , "+sex;
        }
        string getName(){
            return name;
        }
        string getSex(){
            return sex;
        }
        void setName (string n){
            name=n;
        }
        void setSex(string s){
            sex=s;
        }
    };
}

int main(int argc, const char * argv[]) {
    using namespace  minMax;
    using namespace People;
    int a,b;
    cout<<"Inserisci numero"<<endl;
    cin>>a;
    cout<<"Inserisci numero"<<endl;
    cin>>b;
    cout<<endl;
    cout<< min(a,b)<<endl;
    cout<< max (a,b)<<endl;
    cout<<endl;

    Person p ("Diego ","M");
    cout<<p.get()<<endl;
    
    
    return 0;
}
