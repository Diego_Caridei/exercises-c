//
//  Account.h
//  Esercizio1Banca
//
//  Created by Diego Caridei on 27/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//



#include <iostream>
using namespace::std;
class Account{
protected:
    double balance;
public:
    Account(double bal);
    void credit(double amount);
    void debit(double amount);
    double getBalance();
};