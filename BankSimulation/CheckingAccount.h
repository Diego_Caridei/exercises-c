//
//  CheckingAccount.h
//  Esercizio1Banca
//
//  Created by Diego Caridei on 27/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//


#include <iostream>
#include "Account.h"
class CheckingAccount:public Account{
private:
    double fee;
public:
    CheckingAccount(double bal,double f);
    void debit (double amount);
    
};
