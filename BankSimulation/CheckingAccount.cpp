//
//  CheckingAccount.cpp
//  Esercizio1Banca
//
//  Created by Diego Caridei on 27/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include "CheckingAccount.h"
CheckingAccount ::CheckingAccount(double bal,double f):Account(bal){
    fee=f ;
}
void CheckingAccount::debit (double amount){
    if (balance>amount) {
        balance=balance-amount-fee;
    }
    else{
        cout<<"Fondi insufficienti"<<endl;
    }
}
