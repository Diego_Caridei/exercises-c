//
//  main.cpp
//  Esercizio1Banca
//
//  Created by Diego Caridei on 27/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
#include "CheckingAccount.h"

int main(int argc, const char * argv[]) {
    CheckingAccount  mioAccount(500,.5);
    mioAccount.credit(100);
    cout<<mioAccount.getBalance()<<endl;
    mioAccount.debit(50);
    cout<<mioAccount.getBalance()<<endl;;
    return 0;
}
