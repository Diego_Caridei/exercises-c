//
//  Account.cpp
//  Esercizio1Banca
//
//  Created by Diego Caridei on 27/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include "Account.h"
Account::Account(double bal){
    if (bal>0) {
        balance=bal;
    }else{
        bal=0.0;
    }
}
void Account:: credit(double amount){
    balance+=amount;
}
void Account:: debit(double amount){
    if (balance>amount) {
        balance=balance-amount;
    }else{
        cout<<"Fondi insufficienti";
    }
    
}
double Account:: getBalance(){
    return balance;
}


