//
//  Shape.h
//  ClassiAstratte
//
//  Created by Diego Caridei on 28/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
using namespace std;
class Shape {
    virtual void setX(int xcor)=0;
    virtual void setY(int ycor)=0;
    virtual int getY()const =0;
    virtual int getX()const =0;
    virtual void draw() const =0;

};

