//
//  Circle.h
//  ClassiAstratte
//
//  Created by Diego Caridei on 28/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//


#include <iostream>
#include "Shape.h"

class Circle : public Shape{
private:
    int x,y,radius;
public:
    Circle(int xcor,int ycor,int r);
    virtual void setX(int xcor);
    virtual void setY(int ycor);
    virtual void setRadius(int r);
    virtual int getY();
    virtual int getX();
    virtual int getRadius();

    virtual void draw() ;

};
