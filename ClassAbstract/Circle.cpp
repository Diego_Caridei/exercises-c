//
//  Circle.cpp
//  ClassiAstratte
//
//  Created by Diego Caridei on 28/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include "Circle.h"
Circle::Circle(int xcor,int ycor,int r){
    x=xcor;
    y=ycor;
    radius=r;
}
 void Circle::setX(int xcor){
    x=xcor;
}
 void Circle::setY(int ycor){
     y=ycor;
}
void Circle:: setRadius(int r){
    radius=r;
}

int Circle:: getY(){
    return y;
}
int Circle:: getX(){
    return x;
}
int Circle:: getRadius(){
    return radius;
}

void Circle:: draw(){
    cout<<"Drawing circle at: "<< getX()<<" "<<getY()<<"Raggio"<<getRadius()<<endl;
}