//
//  main.cpp
//  VectorDue
//
//  Created by Diego Caridei on 21/11/14.
//  Copyright (c) 2014 Diego Caridei. All rights reserved.
//

#include <iostream>
#include <vector>
using namespace std;

int  sommaVoti(vector<int>vet){
    int totale=0;
    for (int i=0; i<=vet.size(); i++) {
        totale+=vet[i];
    }
    return totale;
}

double calcolaMedia(vector<int>vet){
    int totale;
    double media;
    totale=sommaVoti(vet);
    media=totale/vet.size();
    return media;
}


int main(int argc, const char * argv[]) {
    vector<int>voti;
    int totale;
    double media;
    voti.push_back(20);
    voti.push_back(30);
    voti.push_back(40);
    voti.push_back(50);
    voti.push_back(60);
    voti.push_back(70);
    totale=sommaVoti(voti);
    media=calcolaMedia(voti);
    cout<<"La somma è: "<<totale<<endl;
    cout<<"La media è: "<<media<<endl;
    
    return 0;
}
